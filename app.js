const http = require("http");
const express = require("express");

const app = express();
app.set("view engine", "ejs");

const bodyparser = require("body-parser");

const misRutas = require("./router/index");
const path = require("path");

app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({ extended: true }))
app.use(misRutas);

//Cambiar extensiones ejs a htmln
app.engine("html", require("ejs").renderFile);
const puerto = 500;
app.listen(puerto, () => {
    console.log("Iniciando puerto")
});

//error 
app.use((req, res, next) => {
    res.status(404).sendFile(__dirname + '/public/error.html')
})
